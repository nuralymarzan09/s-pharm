from django.test import SimpleTestCase
from spharm.forms import DeliveryForm


class Test_Form_view(SimpleTestCase):
    def test_delivery_create(self):
        form = DeliveryForm(data={
            'person_name': 'Nuraly Marzhan',
            'person_email': 'nuralymarzan09@gmail.com',
            'person_tel': '87777777777',
            'person_city': 'Astana',
            'person_Address': 'Sauran',
            'person_wish': 'nothing',
            'drug_name': 'name1'
        })
        self.assertTrue(form.is_valid())

    def test_create_form_no_data(self):
        form = DeliveryForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 7)

import unittest
from django.test import TestCase
from django.contrib.auth import get_user_model
from mixer.backend.django import mixer
from unittest import skip
from django.shortcuts import get_object_or_404

from spharm.models import Apteks, Drugs, Delivery, Pharmacies, illness


class IllnessModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        illness.objects.create(illness_name='heart disease')
        print("Illness test created!")

    # Ignore test case, because there is no matching illness name in id = 1
    @unittest.skip
    def test_model_str(self):
        ill = illness.objects.get(id=1)
        field_label = ill._meta.get_field('illness_name').verbose_name
        self.assertEqual(str(field_label), 'heart')


class DrugsModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Drugs.objects.create(name='Big', cost='1200 тг')

    def test_name_label(self):
        drug = Drugs.objects.get(pk=1)
        print("Drug name: ", drug)


User = get_user_model()


class UserTestCast(TestCase):
    def setUp(self):  # Python's builtin unittest
        user_a = User(username='marara', email='nuralymarzan09@gmail.com')
        # User.objects.create()
        # User.objects.create_user()
        user_a_pw = 'marzhan2509'
        self.user_a_pw = user_a_pw
        user_a.is_staff = True
        user_a.is_superuser = True
        user_a.set_password(user_a_pw)
        user_a.save()
        print("User ID: ", user_a.id)

    def test_user_exists(self):
        user_count = User.objects.all().count()
        self.assertEqual(user_count, 1)
        self.assertNotEqual(user_count, 0)
        print("How many users exit: ", user_count)

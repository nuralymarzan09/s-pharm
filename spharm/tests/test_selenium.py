# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# import time
# import os.path
#
# # object of ChromeOptions class
# from selenium.webdriver.common.by import By
#
# op = webdriver.ChromeOptions()
# # browser preferences
# p = {'download.default_directory': 'C:\\Users\\Student01\\Downloads'}
# # add options to browser
# op.add_experimental_option('prefs', p)
# # set chromedriver.exe path
# driver = webdriver.Chrome(executable_path="C:\\Users\\Student01\\Downloads\\chromedriver_win32\\chromedriver.exe",
#                           options=op)
# # maximize browser
# driver.maximize_window()
# # launch URL
# driver.get("http://127.0.0.1:8000/analog")
# # click download link
# l = driver.find_element(By.ID, "export")
# l.click()
# # check if file downloaded file path exists
# while not os.path.exists('C:\\Users\\Student01\\Downloads'):
#     time.sleep(2)
# # check file
# if os.path.isfile('C:\\Users\\Student01\\Downloads'):
#     print("File download is completed")
# else:
#     print("File download is not completed")
# # close browser
# driver.quit()

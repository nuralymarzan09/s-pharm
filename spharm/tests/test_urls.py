from django.test import TestCase
from django.urls import reverse, resolve
from spharm.models import Apteks


class URLPharmTest(TestCase):
    def test_main_page(self):
        response = self.client.get('index')
        print("Checking code of main url: ", response.status_code)
        self.assertEqual(response.status_code, 404)

    def test_get_url(self):
        pharm_1 = Apteks.objects.create(pharm_name='Apteka 1', pharm_city='Almaty')
        pharm_2 = Apteks.objects.create(pharm_name='Apteka 2', pharm_city='Almaty')
        url = reverse('main2')
        print(url)
        response = self.client.get(url)
        print(response)

from django.test import SimpleTestCase
from django.urls import reverse, resolve
from spharm.views import index, index2, delivery, map, drugs, analogies, pharmacy, recommendations, Delivery, \
    model_form_upload


class Test_Views(SimpleTestCase):

    def test_url_1(self):
        url = reverse('main2')
        print("View 1 method: ", resolve(url).func)
        self.assertEqual(resolve(url).func, index2)

    def test_url_2(self):
        url = reverse('del')
        print("View 2 method: ", resolve(url).func)
        self.assertEqual(resolve(url).func, Delivery)

    def test_url_3(self):
        url = reverse('all_drug')
        print("View 3 method: ", resolve(url).func)
        self.assertEqual(resolve(url).func, drugs)

    def test_url_4(self):
        url = reverse('analog')
        print("View 4 method: ", resolve(url).func)
        self.assertEqual(resolve(url).func, analogies)

    def test_url_5(self):
        url = reverse('partner')
        print("View 5 method: ", resolve(url).func)
        self.assertEqual(resolve(url).func, model_form_upload)

    def test_url_6(self):
        url = reverse('recommend')
        print("View 6 method: ", resolve(url).func)
        self.assertEqual(resolve(url).func, recommendations)

    def test_url_7(self):
        url = reverse('map')
        print("View 7 method: ", resolve(url).func)
        self.assertEqual(resolve(url).func, map)

    def test_page_template(self):
        response = self.client.get(reverse('main'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='main/index.html')

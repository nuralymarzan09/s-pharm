import pytest
from django.test import TestCase
from spharm.models import Drugs, Apteks, Delivery


@pytest.fixture
def drugs(db) -> Drugs:
    return Drugs.objects.create(name="Drug 1", cost="1230 тг", pharmacy="Hippokrat 1")


# Parametrizing fixtures

@pytest.fixture(params=("Almaty", "Astana"))
def pharm_one(db, request):
    return Apteks.objects.create(pharm_name="Europharma", pharm_city=request.param)


def test_pharm_city(pharm_one):
    assert all(letter.isalnum() for letter in pharm_one.pharm_city)
    assert len(pharm_one.pharm_city) == 6


# Autouse Fixtures
@pytest.fixture
def delivery_abc(db):
    return Delivery.objects.create(person_name="Test test")


@pytest.fixture
def deliveries(db) -> list:
    return []


# @pytest.fixture(autouse=True)
# def append_deliveries(deliveries, delivery_abc):
#     return deliveries.append(delivery_abc)
